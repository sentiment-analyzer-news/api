from django.shortcuts import render
from django.http import HttpResponse, JsonResponse
from django.views.decorators.csrf import csrf_exempt
import requests
import feedparser
import os
import openai
from environ import Env
from dotenv import load_dotenv
from google.cloud import language_v1
import urllib.request
import json
import yfinance as yf


load_dotenv()




# Create your views here.

@csrf_exempt
def analyze_sentiment(request):
    if request.method == 'GET':
        source = request.GET.get('source')
        if source == 'tradingeconomics':
            country = request.GET.get('') 
            url = 'https://tradingeconomics.com/united-states/rss'
            rss_feed_titles = _get_rss_feed(url)
            print(rss_feed_titles)
        elif source == 'googlenews':
            keyword = request.GET.get('keyword') 
            api_key = os.environ.get("GOOGLE_CUSTOM_SEARCH_API_KEY")
            siteSearch = 'news.google.com'
            dateRestrict = 'd1' 
            num = 10
            url = f'https://www.googleapis.com/customsearch/v1?key={api_key}&cx=61fc6c5bafe504907&q={keyword}&dateRestrict={dateRestrict}&num={num}&siteSearch={siteSearch}'
            #url = 'https://news.google.com/rss/search?q=' + keyword + '&when:24h&site:reuters.com'
            if keyword is None:
                return JsonResponse({'status': 'error', 'response': 'Missing keyword'})
            rss_feed_titles = _get_head_titles_google_news(url)
            #rss_feed_titles = _get_rss_feed(url)
        elif source == 'investing':
            theme = request.GET.get('theme')
            url = 'https://www.investing.com/rss/stock_Indices.rss'
            rss_feed_titles =  _get_rss_feed(url)
        else:
            return JsonResponse({'status': 'error', 'response': 'Invalid source'})
        sentiments = []
        for index, title in enumerate(rss_feed_titles):
            sentiments.append(_analyze_sentiment_with_google(title, source))
        # Return response
        return JsonResponse({'status': 'success', 'response': sentiments})
    
@csrf_exempt
def get_last_price(request):
    if request.method == 'GET':
        # Créez un objet Ticker pour l'action Apple
        ticker = yf.Ticker(request.GET.get('symbol'))
        # Obtenez les données en temps réel (cours actuel)
        data = ticker.history(period="1d")
        # Extrait le dernier prix de clôture
        last_price = data['Close'].iloc[0]
        return JsonResponse(last_price, safe=False)
        """
        symbol = request.GET.get('symbol') #GW7FKNNI075IRQ4M
        url = 'https://www.alphavantage.co/query?function=GLOBAL_QUOTE&symbol=' + symbol + '&apikey=' + os.environ.get("ALPHA_VANTAGE_API_KEY")
        response = requests.get(url)
        data = response.json()
        last_price = data.get('Global Quote').get('05. price')
        return JsonResponse({'status': 'success', 'response': last_price})
        """
        
    

def _get_head_titles_google_news(url):
    response = requests.get(url)
    items = response.json().get('items')
    head_titles = []
    for item in items:
        title = item.get('title')
        link = item.get('link')
        head_titles.append({'text': title, 'link': link})
    return head_titles

    
def _get_rss_feed(url):

    # Send http request to google rss feed
    response = requests.get(
        url)
    feed_content = response.text
    feed = feedparser.parse(feed_content)
    entries = feed.entries

    # Extract the content of a specific tag for each entry
    title_link_dict_list = []
    for entry in entries:
        # Access the desired tags
        title = entry['title'] 
        link = entry['link']
        if 'description' in entry:
            description = entry['description']
        else:
            description = ''
        # Process the tag content as needed
        title_link_dict = { 'text': title, 'link': link, 'description': description }
        title_link_dict_list.append(title_link_dict)
        title_link_dict_list = title_link_dict_list[:10]
        #tag_content_list_to_string = _array_to_string(tag_content_list)
        #test =_get_sentiment_openai(tag_content_list_to_string)
    return title_link_dict_list



def _analyze_sentiment_with_google(title, source):
    # Instantiates a client
    client = language_v1.LanguageServiceClient()

    # The text to analyze
    if source == 'tradingeconomics':
        text = title.get('description')
    else:
        text = title.get('text')
    document = language_v1.types.Document(
        content=text, type_=language_v1.types.Document.Type.PLAIN_TEXT
    )

    # Detects the sentiment of the text
    sentiment = client.analyze_sentiment(
        request={"document": document}
    ).document_sentiment

    return  {
        "title": title,
        "score": round(sentiment.score, 2),
        "magnitude": round(sentiment.magnitude, 2)
    }

def _get_sentiment_openai(text):
    prompt="Rate the sentiment of the following headlines from 1 to 5 with 1 for very negative and 5 for very positive, can you answer in this format [sentence number]:[rating] : \n" + text 
    openai.api_key = os.environ.get("OPENAI_API_KEY")
    response = openai.Completion.create(
        engine="text-davinci-003",
        prompt=prompt,
        temperature=0,
        max_tokens=2000,
        top_p=1,
        frequency_penalty=0,
        presence_penalty=0
    )
    #completions = [choice.text.strip() for choice in response.choices]
    completion = response.choices[0].text.strip()
    return  completion

def _array_to_string(array):
    # List every element of the array separated by a new line
    
    string = ""

    for index, item in enumerate(array):
        if index == 0:
            string += "- " + item + " \n"
        if index > 0 and index < len(array) - 1:
            string += item + " \n- "
        if index == len(array) - 1:
            string += item + " \n"
    return string

